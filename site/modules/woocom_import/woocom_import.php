<?php
/**
 * WooCommerce Data Importer
 *
 * Extends shop capability to allow imports from WooCommers sites.
 *
 * Author: Mladen Mijatov
 */

use Core\Events;
use Core\Module;

use Modules\Shop\Item\Manager as ShopItemManager;


class woocom_import extends Module {
	private static $_instance;

	/**
	 * List of manufacturers.
	 * @var array
	 */
	private $manufacturers = array(
		'CABAIA', 'Moleskine'
	);

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		Events::connect('backend', 'add-menu-items', 'add_menu_items', $this);
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
		self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
		if (isset($params['backend_action']))
			switch ($params['backend_action']) {
				case 'form':
					$this->show_import_form();
					break;

				case 'import':
					$this->handle_import();
					break;

				default:
					break;
			}
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}

	/**
	 * Add backend menu items.
	 */
	public function add_menu_items() {
		$backend = backend::get_instance();
		$import_menu = $backend->getMenu('shop_import');

		$import_menu->addChild(null, new backend_MenuItem(
			$this->get_language_constant('menu_import'),
			$this->path.'images/import.svg',
			window_Open( // on click open window
				'shop_import_woo',
				350,
				$this->get_language_constant('title_import'),
				true, true,
				backend_UrlMake($this->name, 'form')
			),
			5
		));
	}

	/**
	 * Show form for file selection.
	 */
	private function show_import_form() {
		$template = new TemplateHandler('import_form.xml', $this->path.'templates/');
		$template->set_mapped_module($this->name);

		$params = array(
					'form_action'	=> backend_UrlMake($this->name, 'import'),
				);

		$template->restore_xml();
		$template->set_local_params($params);
		$template->parse();
	}

	/**
	 * Import provided file though POST request.
	 */
	private function handle_import() {
		$file_name = $_FILES['data']['tmp_name'];

		if (empty($file_name)) {
			$template = new TemplateHandler('message.xml', $this->path.'templates/');
			$template->set_mapped_module($this->name);

			$params = array(
				'message' => $this->get_language_constant('message_invalid_file'),
				'name'    => $file_name,
				'button'  => $this->get_language_constant('close'),
				'action'  => window_Close('shop_import_woo')
			);

			$template->restore_xml();
			$template->set_local_params($params);
			$template->parse();

		} else {
			$imported = $this->import_from_file($file_name);

			$template = new TemplateHandler('message.xml', $this->path.'templates/');
			$template->set_mapped_module($this->name);

			$params = array(
				'message' => $this->get_language_constant('message_imported'),
				'name'    => $imported,
				'button'  => $this->get_language_constant('close'),
				'action'  => window_Close('shop_import_woo')
			);

			$template->restore_xml();
			$template->set_local_params($params);
			$template->parse();
		}
	}

	/**
	 * Import data from file name. See example file for expected format.
	 *
	 * @param string $file_name
	 * @return integer
	 */
	private function import_from_file($file_name) {
		if (!file_exists($file_name)) {
			error_log('Unable to find file for import: '.$file_name);
			return 0;
		}

		// load and decode data
		try {
			$raw_data = file_get_contents($file_name);
			$data = json_decode($raw_data);
		} catch (Exception $error) {
			error_log((string) $error);
		}

		$item_map = array();
		$related_items = array();

		// create managers
		$gallery = gallery::get_instance();
		$item_manager = ShopItemManager::get_instance();
		$relations_manager = ShopRelatedItemsManager::get_instance();
		$category_manager = ShopCategoryManager::get_instance();
		$manufacturer_manager = ShopManufacturerManager::get_instance();
		$gallery_manager = GalleryManager::get_instance();
		$gallery_group_manager = GalleryGroupManager::get_instance();

		// get all the manufacturers
		$manufacturers = $manufacturer_manager->get_items($manufacturer_manager->get_field_names(), array());
		$total_imported = 0;

		foreach ($data as $item) {
			$manufacturer = 0;

			// populate related items map
			$related_items[$item->id] = $item->related_ids;

			// prepare data
			$item_data = array(
				'name'         => array('he' => $item->name),
				'description'  => array('he' => $item->description),
				'manufacturer' => $manufacturer,
				'price'        => $item->price,
				'weight'       => empty($item->weight) ? 0 : (float) $item->weight,
			);

			// check if item is already in the database
			$existing_item = $item_manager->get_single_item(
				array('id', 'uid', 'gallery'),
				array('uid' => (string) $item->id)
			);

			// insert of update data
			if (is_object($existing_item)) {
				$item_manager->update_items($item_data, array('id' => $existing_item->id));
				$item_map[$item->id] = $existing_item->id;
				$gallery_id = $existing_item->gallery;
			} else {
				$gallery_group_manager->insert_item(array(
					'text_id' => (string) $item->id,
					'name'    => array('he' => $item->name)
				));
				$gallery_id = $gallery_group_manager->get_inserted_id();

				$item_data['uid'] = $item->id;
				$item_data['gallery'] = $gallery_id;
				$item_manager->insert_item($item_data);
				$item_map[$item->id] = $item_manager->get_inserted_id();
			}
			$total_imported++;

			// update item images
			$gallery_manager->delete_items(array('group' => $gallery_id));

			if (count($item->images) > 0 && false)  // TODO: Disabled temporarily.
				foreach ($item->images as $image) {
					$file_name = basename($image->src);
					$tmp_file_name = tempnam(sys_get_temp_dir(), 'import');
					$raw_image = file_get_contents($image->src);
					file_put_contents($tmp_file_name, $raw_image);
					$file_size = strlen($raw_image);

					$_FILES['import_image'] = array(
						'name'     => $file_name,
						'tmp_name' => $tmp_file_name,
						'size'     => $file_size
					);
					$image_id = $gallery->create_image('import_image', 0);
					$gallery_manager->update_items(
						array(
							'group' => $gallery_id,
							'title' => array('he' => $image->name)
						),
						array('id' => $image_id)
					);
				}
		}

		// updated related items
		foreach ($related_items as $item_id => $related_list) {
			if (!in_array($item_id, $item_map))
				continue;

			$our_item_id = $item_map[$item_id];
			$relations_manager->delete_items(array('item' => $our_item_id));

			foreach ($related_list as $related_id) {
				if (!in_array($related_id, $item_map))
					continue;
				$our_related_id = $item_map[$related_id];
				$relations_manager->insert_data(array(
					'item'    => $our_item_id,
					'related' => $our_related_id
				));
			}
		}

		return $total_imported;
	}
}
