<?php

/**
 * Shop payment method base class
 */
use Core\Events;


class Cardcom_PaymentMethod extends PaymentMethod {
	private static $_instance;

	protected $name;
	protected $parent;
	protected $checkout_url;

	private $currency_map = array(
		'ILS' => 1, 'USD' => 2, 'AUD' => 36, 'CAD' => 124, 'DKK' => 208, 'JPY' => 392,
		'NZD' => 554, 'RUB' => 643, 'CHF' => 756, 'GBP' => 826, 'USD' => 840, 'EUR' => 978,
	);

	const API_ENDPOINT = 'https://secure.cardcom.solutions/Interface/LowProfile.aspx';


	protected function __construct($parent) {
		parent::__construct($parent);

		// register payment method
		$this->name = 'cardcom';
		$this->registerPaymentMethod();
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance($parent) {
		if (!isset(self::$_instance))
			self::$_instance = new self($parent);

		return self::$_instance;
	}

	/**
	 * Whether this payment method requires system to ask user for credit
	 * card information.
	 *
	 * @return boolean
	 */
	public function needs_credit_card_information() {
		return false;
	}

	/**
	 * If recurring payments are supported by this payment method.
	 * @return boolean
	 */
	public function supports_recurring() {
		return false;
	}

	/**
	 * If delayed payments are supported by this payment method.
	 *
	 * @return boolean
	 */
	public function supports_delayed() {
		return false;
	}

	/**
	 * Get URL to be used in checkout form.
	 * @return string
	 */
	public function get_url() {
		return $this->checkout_url;  // populated by new_payment methods which are called prior to this
	}

	/**
	 * Get display name of payment method
	 * @return string
	 */
	public function get_title() {
		return $this->parent->get_language_constant('method_title');
	}

	/**
	 * Get icon URL for payment method
	 * @return string
	 */
	public function get_icon_url() {
		return URL::from_file_path($this->parent->path.'images/icon.svg');
	}

	/**
	 * Get image URL for payment method
	 * @return string
	 */
	public function get_image_url() {
		return URL::from_file_path($this->parent->path.'images/image.png');
	}

	/**
	 * Get list of plans for recurring payments.
	 *
	 * Plan groups, if not empty, are used to group plans. When creating
	 * new recurring payment all plans from the same group will be canceled.
	 *
	 * @return array
	 */
	public function get_recurring_plans() {
		return array();
	}

	/**
	 * Get billing information from payment method.
	 *
	 * $result = array(
	 * 			'first_name'	=> '',
	 * 			'last_name'		=> '',
	 * 			'email'			=> ''
	 * 		);
	 *
	 * @return array
	 */
	public function get_information() {
		return array(
			'first_name' => '',
			'last_name'  => '',
			'email'      => ''
		);
	}

	/**
	 * Make new payment form with specified items and return
	 * hidden elements for posting to URL.
	 *
	 * @param array $transaction_data
	 * @param array $billing_information
	 * @param array $items
	 * @param string $return_url
	 * @param string $cancel_url
	 * @return string
	 */
	public function new_payment($transaction_data, $billing_information, $items, $return_url, $cancel_url) {
		global $language;

		// get configured currency
		$currency_id = null;
		$default_currency = shop::getDefaultCurrency();
		if (array_key_exists($default_currency, $this->currency_map))
			$currency_id = $this->currency_map[$default_currency];

		// prepare data for API call
		$total_amount = 0;
		$total_amount += $transaction_data['handling'];
		$total_amount += $transaction_data['shipping'];
		$total_amount += $transaction_data['total'];

		$request_params = array(
			'Operation'          => 1,  // billing only
			'TerminalNumber'     => $this->parent->settings['terminal_number'],
			'UserName'           => $this->parent->settings['user_name'],
			'SumToBill'          => $total_amount,
			'CoinId'             => $currency_id,
			'Language'           => $language,
			'ProductName'        => str_replace(
					'{#}', $transaction_data['uid'],
					$this->parent->get_language_constant('order_number')
				),
			'APILevel'           => 10,
			'Codepage'           => 65001,
			'SuccessRedirectUrl' => $return_url,
			'ErrorRedirectUrl'   => $cancel_url,
			'IndicatorUrl'       => URL::make_query('cardcom', 'process-notification'),  // notification URL
			'ReturnValue'        => $transaction_data['uid']
		);

		// make api call
		$handle = curl_init(self::API_ENDPOINT);
		curl_setopt($handle, CURLOPT_POST, true);
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($request_params));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		$raw_response = curl_exec($handle);
		curl_close($handle);

		$response = array();
		parse_str($raw_response, $response);

		if (isset($response['ResponseCode']) && $response['ResponseCode'] == '0') {
			// successful data preparation, assign url
			$this->checkout_url = $response['url'];

		} else {
			// problem with generating checkout url, redirect user to cancel page and log it
			error_log('Unable to generate checkout URL: '.$response['Description']);
			$this->checkout_url = $cancel_url;
		}

		return '';
	}

	/**
 	 * Make nwe delayed payment form with specified items and return
	 * hidden elements for posting to URL.
	 *
	 * @param array $transaction_data
	 * @param array $billing_information
	 * @param array $items
	 * @param string $return_url
	 * @param string $cancel_url
	 * @return string
	 */
	public function new_delayed_payment($transaction_data, $billing_information, $items, $return_url, $cancel_url) {
		return '';
	}

	/**
	 * Make new recurring payment based on named plan and return
	 * hidden elements for posting to URL.
	 *
	 * @param array $transaction_data
	 * @param array $billing_information
	 * @param string $plan_name
	 * @param string $return_url
	 * @param string $cancel_url
	 * @return string
	 */
	public function new_recurring_payment($transaction_data, $billing_information, $plan_name, $return_url, $cancel_url) {
		return '';
	}

	/**
	 * Cancel existing recurring payment.
	 *
	 * @param object $transaction
	 * @return boolean
	 */
	public function cancel_recurring_payment($transaction) {
		return false;
	}

	/**
	 * Charge delayed transaction.
	 *
	 * @param object $transaction
	 * @return boolean
	 */
	public function charge_transaction($transaction) {
		return false;
	}

	/**
	 * Handle notification from the provider about completed transaction.
	 */
	public function transaction_notification() {
		// transaction must exist in our system
		if (!isset($_GET['ReturnValue']))
			return;

		$transaction_id = fix_chars($_GET['ReturnValue']);
		$transaction = Transaction::get($transaction_id);
		if (!is_object($transaction))
			return;

		// allow changing status through notification only once to avoid problems
		if ($transaction->status != TransactionStatus::PENDING)
			return;

		// update remote transaction id
		if (isset($_GET['LowProfileCode'])) {
			$remote_transaction_id = fix_chars($_GET['LowProfileCode']);
			Transaction::set_remote_id($transaction, $remote_transaction_id);
		}

		// update transaction status
		switch ($transaction->type) {
			case TransactionType::REGULAR:
				if (isset($_GET['OperationResponse']) && $_GET['OperationResponse'] == 0)
					$status = TransactionStatus::COMPLETED; else
					$status = TransactionStatus::DENIED;
				break;

			default:
				$status = null;
				break;
		}

		if (!is_null($status)) {
			$this->parent->setTransactionStatus($transaction->uid, $status);
		}
	}
}
