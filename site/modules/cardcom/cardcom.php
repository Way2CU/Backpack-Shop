<?php
/**
 * CardCom Payment Method
 *
 * Author: Mladen Mijatov
 */

use Core\Events;
use Core\Module;


class cardcom extends Module {
	private static $_instance;
	private $method;

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		Events::connect('backend', 'add-menu-items', 'add_menu_items', $this);

		if (ModuleHandler::is_loaded('shop')) {
			require_once('units/payment_method.php');
			$this->method = Cardcom_PaymentMethod::get_instance($this);
		}
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
		self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
		// global control action
		if (isset($params['action']))
			switch ($params['action']) {
				case 'process-notification':
					$this->method->transaction_notification();
					break;

				default:
					break;
			}

		if (isset($params['backend_action']))
			switch ($params['backend_action']) {
				case 'show-settings':
					$this->show_settings();
					break;

				case 'save-settings':
					$this->save_settings();
					break;

				default:
					break;
			}
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
		$this->save_setting('terminal_number', '');
		$this->save_setting('user_name', '');
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}

	/**
	 * Add backend menu items.
	 */
	public function add_menu_items() {
		$backend = backend::get_instance();
		$method_menu = $backend->getMenu('shop_payment_methods');

		$method_menu->addChild(null, new backend_MenuItem(
			$this->get_language_constant('menu_item'),
			$this->path.'images/icon.svg',
			window_Open( // on click open window
				'shop_configure_cardcom',
				350,
				$this->get_language_constant('title_configure'),
				true, true,
				backend_UrlMake($this->name, 'show-settings')
			),
			5
		));
	}

	/**
	 * Show settings form and allow user to configure this module.
	 */
	private function show_settings() {
		$template = new TemplateHandler('settings.xml', $this->path.'templates/');
		$template->set_mapped_module($this->name);

		$params = array(
						'form_action'	=> backend_UrlMake($this->name, 'save-settings'),
					);

		$template->restore_xml();
		$template->set_local_params($params);
		$template->parse();
	}

	/**
	 * Save new settings.
	 */
	private function save_settings() {
		$template = new TemplateHandler('message.xml', $this->path.'templates/');
		$template->set_mapped_module($this->name);

		$this->save_setting('terminal_number', escape_chars($_REQUEST['terminal_number']));
		$this->save_setting('user_name', escape_chars($_REQUEST['user_name']));

		$params = array(
					'message'	=> $this->get_language_constant('message_settings_saved'),
					'button'	=> $this->get_language_constant('close'),
					'action'	=> window_Close('shop_configure_cardcom')
				);

		$template->restore_xml();
		$template->set_local_params($params);
		$template->parse();
	}
}
