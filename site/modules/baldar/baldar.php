<?php
/**
 * WooCommerce Data Importer
 *
 * Extends shop capability to allow imports from WooCommers sites.
 *
 * Author: Mladen Mijatov
 */

use Core\Events;
use Core\Module;

use Modules\Shop\Item\Manager as ShopItemManager;


class baldar extends Module {
	private static $_instance;
	private $method;

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		Events::connect('backend', 'add-menu-items', 'add_menu_items', $this);

		if (ModuleHandler::is_loaded('shop')) {
			require_once('units/method.php');
			$this->method = Baldar_DeliveryMethod::get_instance($this);
		}
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
		self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
		if (isset($params['backend_action']))
			switch ($params['backend_action']) {
				case 'show-settings':
					$this->show_settings();
					break;

				case 'save-settings':
					$this->save_settings();
					break;

				default:
					break;
			}
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
		$this->save_setting('delivery', 0);
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}

	/**
	 * Add backend menu items.
	 */
	public function add_menu_items() {
		$backend = backend::get_instance();
		$import_menu = $backend->getMenu('shop_delivery_methods');

		$import_menu->addChild(null, new backend_MenuItem(
			$this->get_language_constant('menu_configure'),
			$this->path.'images/icon.svg',
			window_Open( // on click open window
				'shop_delivery_baldar',
				350,
				$this->get_language_constant('title_settings'),
				true, true,
				backend_UrlMake($this->name, 'show-settings')
			), 5
		));
	}

	/**
	 * Show settings form and allow user to configure this module.
	 */
	private function show_settings() {
		$template = new TemplateHandler('settings.xml', $this->path.'templates/');
		$template->set_mapped_module($this->name);

		$params = array(
						'form_action'	=> backend_UrlMake($this->name, 'save-settings'),
					);

		$template->restore_xml();
		$template->set_local_params($params);
		$template->parse();
	}

	/**
	 * Save new settings.
	 */
	private function save_settings() {
		$template = new TemplateHandler('message.xml', $this->path.'templates/');
		$template->set_mapped_module($this->name);

		$this->save_setting('delivery', escape_chars($_REQUEST['delivery']));

		$params = array(
					'message'	=> $this->get_language_constant('message_settings_saved'),
					'button'	=> $this->get_language_constant('close'),
					'action'	=> window_Close('shop_delivery_baldar')
				);

		$template->restore_xml();
		$template->set_local_params($params);
		$template->parse();
	}
}
